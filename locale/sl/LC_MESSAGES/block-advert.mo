��          �      <      �  K   �     �          3     K     g     �     �     �     �     �  y   �  �  i     �  �     �   �     �  �  �  U   �  -   �  +   $	  *   P	  $   {	  +   �	  	   �	     �	  &   �	     
     )
  �   I
  �  �
     �  �   �  �   �     6               
                           	                                                           <b> Failed </b> \n\n	antiX advert blocker must be run as root or with sudo  Block Ad and Malware websites Block Fakenews websites Block Gambling websites Block Pornographic websites Block Social Media websites Cancel Choose what to block Loading  blocklist from $domain No item selected Restoring original /etc/hosts. Success - your settings have been changed.\n\nYour hosts file has been updated.\nRestart your browser to see the changes. The <b>$title</b> tool adds stuff to your /etc/hosts file, so \nthat many advertising servers and websites can't connect to this PC.\nYou can choose to block ads, malware, pornography, gambling, fakenews and social media\nBlocking ad servers protects your privacy, saves you bandwidth, greatly \nimproves web-browsing speed and makes the internet much less annoying in general.\n\nDo you want to proceed? UNBLOCK EVERYTHING \n ERROR: \n No /etc/hosts.ORIGINAL was found, so it can't be restored.\n Probably you already UNBLOCKED EVERYTHING. \n You will have to manually edit the file /etc/hosts to remove any unwanted content \n \n \n (NOTE1: This application's main window always opens showing the default selection,\n not what's currently selected. \n  NOTE2: RESTART YOUR BROWSER TO SEE THE EFFECTS OF ANY CHANGE!) antiX Advert Blocker Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2023-11-30 07:49+0100
Last-Translator: Arnold Marko <arnold.marko@gmail.com>, 2019,2022-2023
Language-Team: Slovenian (http://app.transifex.com/anticapitalista/antix-development/language/sl/)
Language: sl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);
X-Generator: Poedit 3.2.2
 <b> Napaka </b> \n\n	antiX blokada oglasov motra biti zagnana kot root ali preko sudo Blokiraj oglasne in škodljive spletne strani Blokiraj spletne strani z lažnimi novicami Blokiraj spletne strani z igrami na srečo Blokiraj pornografske spletne strani Blokiraj spletne strani družabnih omrežji Prekliči Izberite kaj naj se blokira Nalaganje seznama blokiranih z $domain Nič ni izbrano Obnova originalnega /etc/hosts. Sprememba nastavitev je uspela.

Vaše gostiteljska datoteka je bila posodobljena.
Ponovno zaženite spletni brskalnik, da bodo spremembe vidne. Orodje <b>$title</b> doda v vašo /etc/hosts datoteko elemente,
ki mnogim oglaševalskim strežnikom in spletnim stranem onemogočajo
povezovanje s tem računalnikom.

Lahko izberete blokiranje oglasov, škodljive kode, pornografije, iger na srečo, lažnih novic in družabnih omrežji.\nBlokiranje oglaševalskih strežnikov sčiti vašo zasebnost, manjša pretok podatkov,
zelo poveča hitrost brskanja in naredi splet veliko manj zoprn.

Ali želite nadaljevati? ODBLOKIRAJ VSE \n NAPAKA:\n Ker /etc/hosts.ORIGINAL ni bil najden, ne more biti obnovljen.\n Verjetno ste že ODBLOKIRALI VSE. \n Za odstranitev neželene vsebine je potrebno ročno urediti /etc/hosts datoteko. \n \n (OPOMBA: Glavno okno programa ob odprtju vedno prikaže privzeti izbor in ne,\n kar je trenutno izbrano.\n  OPOMBA2: ZA PRIKAZ SPREMEMB JE POTREBNO BRSKALNIK PONOVNO ZAGNATI!) antiX blokada oglasov 